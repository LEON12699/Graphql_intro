"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const data_store_1 = require("./../data/data.store");
const lodash_1 = __importDefault(require("lodash"));
const mutation = {
    Mutation: {
        cursoNuevo(__, { curso }) {
            const itemCurso = {
                id: String(data_store_1.base.cursos.length + 1),
                title: curso.title,
                description: curso.description,
                clases: curso.clases,
                time: curso.time,
                level: curso.level,
                logo: curso.logo,
                path: curso.path,
                teacher: curso.teacher,
                reviews: []
            };
            if (data_store_1.base.cursos.filter(itemC => itemC.title == itemCurso.title).length === 0) {
                data_store_1.base.cursos.push(itemCurso);
                return itemCurso;
            }
            return {
                id: '-1',
                title: `EL CURSO YA EXISTE CON ESTE TITULO`,
                description: "",
                clases: -1,
                time: 0.0,
                level: "TODOS",
                logo: "",
                path: "",
                teacher: "",
                reviews: []
            };
        },
        modificarCurso(__, { curso }) {
            const elementoExiste = lodash_1.default.findIndex(data_store_1.base.cursos, function (o) {
                return o.id === curso.id;
            });
            if (elementoExiste > -1) {
                const valoraciones = data_store_1.base.cursos[elementoExiste].reviews;
                curso.reviews = valoraciones;
                data_store_1.base.cursos[elementoExiste] = Object.assign(Object.assign({}, data_store_1.base.cursos[elementoExiste]), curso);
                return curso;
            }
            return {
                id: '-1',
                title: `EL CURSO NO EXISTE EN LA BASE DE DATOS NO ES POSIBLE MODIFICAR`,
                description: "",
                clases: -1,
                time: 0.0,
                level: "TODOS",
                logo: "",
                path: "",
                teacher: "",
                reviews: []
            };
        },
        eliminarCurso(__, { idCurso }) {
            const borrarCurso = lodash_1.default.remove(data_store_1.base.cursos, function (curso) {
                return curso.id === idCurso;
            });
            console.log(borrarCurso);
            if (borrarCurso[0] === undefined) {
                return {
                    id: '-1',
                    title: `EL CURSO NO SE PUEDE BORRAR PORQUE NO SE A ENCONTRADO NINGUN CURSO CON EL ID`,
                    description: "",
                    clases: -1,
                    time: 0.0,
                    level: "TODOS",
                    logo: "",
                    path: "",
                    teacher: "",
                    reviews: []
                };
            }
            return borrarCurso[0];
        }
    }
};
exports.default = mutation;
