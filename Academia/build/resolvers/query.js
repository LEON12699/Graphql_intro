"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const data_store_1 = require("../data/data.store");
const query = {
    Query: {
        estudiantes() {
            return data_store_1.base.estudiantes;
        },
        estudiante(__, { id }) {
            const result = data_store_1.base.estudiantes.filter(estudiante => estudiante.id === id)[0];
            if (result == undefined) {
                return {
                    id: '-1',
                    name: `no se a encontado el estudiante con el ID:${id}`,
                    email: "",
                    courses: []
                };
            }
            return result;
        },
        cursos() {
            return data_store_1.base.cursos;
        },
        curso(__, { id }) {
            const result = data_store_1.base.cursos.filter(curso => curso.id === id)[0];
            if (result == undefined) {
                return {
                    id: '-1',
                    name: `no se a encontado el estudiante con el ID:${id}`,
                    description: '',
                    clases: -1,
                    time: 0.0,
                    logo: '',
                    level: "TODOS",
                    path: '',
                    teacher: '',
                    reviews: [],
                    title: ''
                };
            }
            return result;
        },
    }
};
exports.default = query;
