/*
 * @Author: Your name
 * @Date:   2020-04-15 21:05:06
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-17 00:34:41
 */
import cursos from './courses.json';
import estudiantes from './students.json';

export const base={
    cursos,
    estudiantes
}
