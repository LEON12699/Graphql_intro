
/*
 * @Author: Your name
 * @Date:   2020-04-15 22:37:45
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-20 00:11:24
 */
import {IResolvers} from 'graphql-tools';
import { base } from './../data/data.store';
import _ from 'lodash';

const mutation : IResolvers={
    Mutation:{
        cursoNuevo(__:void,{curso}):any{
            const itemCurso={
                id:String(base.cursos.length+1),
                title:curso.title,
                description:curso.description,
                clases:curso.clases,
                time:curso.time,
                level:curso.level,
                logo:curso.logo,
                path:curso.path,
                teacher:curso.teacher,
                reviews:[]
            };
            if(base.cursos.filter(itemC=>itemC.title==itemCurso.title).length === 0){
                base.cursos.push(itemCurso);
                return itemCurso;
                
            }
            
            return {
                id:'-1',
                title:`EL CURSO YA EXISTE CON ESTE TITULO`,
                description:"",
                clases:-1,
                time:0.0,
                level:"TODOS",
                logo:"",
                path:"",
                teacher:"",
                reviews:[]
            };
        },
        modificarCurso(__:void,{curso}):any{
            const elementoExiste=_.findIndex(base.cursos,function(o){
                return o.id === curso.id
            })
            
            if (elementoExiste>-1){
                const valoraciones= base.cursos[elementoExiste].reviews;
                curso.reviews = valoraciones
                base.cursos[elementoExiste] = { ...base.cursos[elementoExiste],...curso};
                return curso;
            }

            return {
                id:'-1',
                title:`EL CURSO NO EXISTE EN LA BASE DE DATOS NO ES POSIBLE MODIFICAR`,
                description:"",
                clases:-1,
                time:0.0,
                level:"TODOS",
                logo:"",
                path:"",
                teacher:"",
                reviews:[]
            };
        },
        eliminarCurso(__:void,{idCurso}):any{
            const borrarCurso =_.remove(base.cursos,function(curso){
                return curso.id === idCurso
            })

            console.log(borrarCurso)
            if(borrarCurso[0] === undefined){
                return {
                    id:'-1',
                    title:`EL CURSO NO SE PUEDE BORRAR PORQUE NO SE A ENCONTRADO NINGUN CURSO CON EL ID`,
                    description:"",
                    clases:-1,
                    time:0.0,
                    level:"TODOS",
                    logo:"",
                    path:"",
                    teacher:"",
                    reviews:[]
                };
            }

            return borrarCurso[0];
        }
    }
}



export default mutation;