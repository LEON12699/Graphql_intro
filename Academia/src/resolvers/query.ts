
/*
 * @Author: Your name
 * @Date:   2020-04-15 22:37:45
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-17 00:08:04
 */
import {IResolvers} from 'graphql-tools';
import { base } from '../data/data.store'

const query : IResolvers={
    Query:{
        estudiantes():any{
            return base.estudiantes
        },
        estudiante(__:void,{id}):any{
            const result= base.estudiantes.filter(estudiante=> estudiante.id ===id)[0]
                if (result==undefined){
                return{
                    id:'-1',
                    name:`no se a encontado el estudiante con el ID:${id}`,
                    email:"",
                    courses:[]
                }
            }
            return result    
        },
        cursos():any{
            return base.cursos
        },
        curso(__:void,{id}):any{
            const result= base.cursos.filter(curso=> curso.id ===id)[0]
                if (result==undefined){
                return{
                    id:'-1',
                    name:`no se a encontado el estudiante con el ID:${id}`,
                    description :'',
                    clases:-1,
                    time:0.0,
                    logo:'',
                    level:"TODOS",
                    path:'',
                    teacher:'',
                    reviews:[],
                    title:''
                }
            }
            return result    
        },
    }
}

export default query;