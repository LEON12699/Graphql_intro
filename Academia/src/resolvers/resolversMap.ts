/*
 * @Author: Your name
 * @Date:   2020-04-15 22:39:11
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-19 23:17:10
 */

import {IResolvers} from 'graphql-tools';
import  query  from './query';
import  type  from './type';
import mutation from './mutation';

const resolversMap : IResolvers={
    ...query,
    ...mutation,
    ...type,
    
}

export default resolversMap;

