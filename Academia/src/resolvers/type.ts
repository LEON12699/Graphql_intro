
/*
 * @Author: Your name
 * @Date:   2020-04-15 22:37:45
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-16 23:58:25
 */
import {IResolvers} from 'graphql-tools';
import { base } from '../data/data.store'
import _ from 'lodash'

const type : IResolvers={
    Estudiante:{
        courses:parent =>{
            const cursosLista : Array<any>=[];
            parent.courses.map((curso:string)=>{
                cursosLista.push(_.filter(base.cursos,['id',curso])[0])
            })
        return cursosLista;
        }
        
    },
    Curso:{
        students:parent=>{
            const listaEstudiantes:Array<any>=[];
            const idCurso= parent.id;
            base.estudiantes.map((estudiante:any)=>{
                if(estudiante.courses.filter((id:any)=>id === idCurso)>0){
                    listaEstudiantes.push(estudiante)
                }
            })
            return listaEstudiantes;
        },
        path:parent=>`https://www.udemy.com${parent.path}`
    }
}



export default type;