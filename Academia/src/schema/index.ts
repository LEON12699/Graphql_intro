/*
 * @Author: Your name
 * @Date:   2020-04-15 22:42:12
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-15 22:42:56
 */
import 'graphql-import-node' // imput de graphql de ficheros
import typeDefs from './schema.graphql'
import { GraphQLSchema } from 'graphql'
import { makeExecutableSchema } from 'graphql-tools'
import resolvers from '../resolvers/resolversMap'
const schema: GraphQLSchema =makeExecutableSchema({
    typeDefs,
    resolvers
})

export default schema