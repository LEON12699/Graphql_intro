/*
 * @Author: Your name
 * @Date:   2020-04-15 21:12:07
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-21 02:51:00
 * dependency for production  graphql-tag-pluck
 */
import {ApolloServer } from 'apollo-server-express'
import express from 'express';
import compression from 'compression';
import cors from 'cors';
import { createServer } from 'http';
import schema from './schema';
import expressPlayGround from 'graphql-playground-middleware-express'

const app = express()

app.use('*', cors())

app.use(compression())
const servidor = new ApolloServer({
    schema,
    introspection:true
})
servidor.applyMiddleware({app});

app.get('/',expressPlayGround({
    endpoint:'/graphql'
}))

const httpserver = createServer(app)
const PORT = 8089
httpserver.listen({
    port: PORT
},
    () => console.log(`SERVIDOR DE ACADEMIA ONLINE http://localhost:${PORT}`)
)