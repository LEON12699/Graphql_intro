"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const query = {
    Query: {
        hola() {
            return 'Hola mundo';
        },
        holaConNombre(__, { nombre }) {
            return `Hola mundo ${nombre}`;
        },
        holaDefinido() {
            return 'Hola Definido';
        }
    }
};
exports.default = query;
