/*
 * @Author: Your name
 * @Date:   2020-04-15 01:06:11
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-15 01:08:04
 */
import { IResolvers } from "graphql-tools";

const query:IResolvers={
    Query:{
        hola():string{
            return 'Hola mundo';
        },
        holaConNombre(__:void,{nombre}):string{
            return `Hola mundo ${nombre}`
        },
        holaDefinido():string{
            return 'Hola Definido';
        }
    }
}

export default query;