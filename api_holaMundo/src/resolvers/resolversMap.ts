/*
 * @Author: Your name
 * @Date:   2020-04-15 01:08:17
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-15 01:08:17
 */
import { IResolvers } from "graphql-tools";
import  query  from "./query";

const resolvers:IResolvers={
    ...query
}

export default resolvers;