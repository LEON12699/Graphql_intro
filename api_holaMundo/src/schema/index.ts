/*
 * @Author: Your name
 * @Date:   2020-04-15 01:02:04
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-15 01:10:03
 */

import { GraphQLSchema } from "graphql";
import { makeExecutableSchema } from "graphql-tools";
import 'graphql-import-node';
import typeDefs from './schema.graphql'
import resolvers from '../resolvers/resolversMap'

const schema: GraphQLSchema=makeExecutableSchema({
    typeDefs,
    resolvers
})

export default schema;