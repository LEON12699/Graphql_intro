/*
 * @Author: Your name
 * @Date:   2020-04-15 00:32:47
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-21 02:32:57
 */
import express from 'express'
import compression from 'compression';
import cors from 'cors'
import schema from './schema'
//import graphQLHTTP from 'express-graphql'
import { ApolloServer } from "apollo-server-express";
import { createServer } from 'http';
import expressPlayground from 'graphql-playground-middleware-express'
const app= express();
const server = new ApolloServer({
    schema,
    introspection:true
})

server.applyMiddleware({app})

app.use('*',cors());

app.use(compression());


app.use('/',expressPlayground({
    endpoint: '/graphql'
}))

/*
app.use('/',graphQLHTTP({
    schema,
    graphiql:true
}));
*/



const PORT= 8089;
const HTTPServer= createServer(app);
app.listen(
    {port:PORT},
    ()=> console.log(`Hola mundo Api Graphql http://localhost:${PORT}/graphql`)
)