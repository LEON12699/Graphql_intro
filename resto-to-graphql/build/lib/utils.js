"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getWikipediaMobileURL(url) {
    return (url !== undefined) ? url.replace('wikipedia', "m.wikipedia") : "";
}
exports.getWikipediaMobileURL = getWikipediaMobileURL;
function checkYear(year) {
    const currentYear = new Date().getFullYear();
    if (isNaN(+year) || +year < 1950 || +year > currentYear) {
        return String(currentYear);
    }
    return year;
}
exports.checkYear = checkYear;
function roundCheck(round) {
    if (round >= 100) {
        return 1;
    }
    return round;
}
exports.roundCheck = roundCheck;
