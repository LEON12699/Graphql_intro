
/*
 * @Author: Your name
 * @Date:   2020-04-20 21:22:03
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-21 01:42:11
 */
import { F1 } from './data-source';

export class CircuitsData extends F1 {
    constructor(){
        super();
        
    }

    async getCircuits(pageElements:number=-1,page:number=1){
        if (pageElements===-1){
        return await this.get(`circuits.json?limit=1000`,{
            cacheOptions:{ttl:60}
        });
    }else{
        const offset=(page-1) * pageElements
        const limit = pageElements;
        const filter =`limit=${limit}&offset=${offset}`;
        return await this.get(`circuits.json?${filter}`,{
            cacheOptions:{ttl:60}
        });
    }

    }

    async getcircuitByID(id:string){
        return this.get((`/circuits/${id}.json`),{
            cacheOptions:{ttl:60}
        }
        )
    }

    
}