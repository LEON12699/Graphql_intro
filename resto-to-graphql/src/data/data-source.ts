/*
 * @Author: Your name
 * @Date:   2020-04-20 21:22:03
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-20 21:33:19
 */
import {RESTDataSource} from 'apollo-datasource-rest';

export class F1 extends RESTDataSource {
    constructor(){
        super()
        this.baseURL='https://ergast.com/api/f1/';
        
    }

}