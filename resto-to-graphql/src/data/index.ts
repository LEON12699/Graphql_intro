import { CircuitsData } from './data-circuits';
/*
 * @Author: Your name
 * @Date:   2020-04-20 21:24:42
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-21 01:31:50
 */
import { RacesData } from './data-races';
import { SeasonsData } from "./data-seasons";
import { DriversData } from './data-drivers';


export const dataSource ={
    SeasonsData,
    RacesData,
    DriversData,
    CircuitsData
}