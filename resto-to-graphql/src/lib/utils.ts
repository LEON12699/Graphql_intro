/*
 * @Author: Your name
 * @Date:   2020-04-20 22:04:31
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-20 23:30:31
 */
export function getWikipediaMobileURL(url:string){
    return (url !== undefined) ? url.replace('wikipedia',"m.wikipedia"):""
}

export function checkYear(year:string){
    const currentYear = new Date().getFullYear();
        if (isNaN(+year) || +year<1950 || +year>currentYear){
            return String(currentYear)
        }
    return year
}

export function roundCheck(round:number){
    if (round>=100){
        return 1;
    }
    return round;
}