/*
 * @Author: Your name
 * @Date:   2020-04-20 21:56:08
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-20 23:36:42
 */
import { IResolvers } from 'graphql-tools';
import query from './query';
import type from './type';

const resolvers : IResolvers = {
    ...query,
    ...type
};

export default resolvers;