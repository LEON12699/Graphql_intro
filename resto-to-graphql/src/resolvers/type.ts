/*
 * @Author: Your name
 * @Date:   2020-04-20 21:55:28
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-21 01:29:29
 */

import { IResolvers } from 'graphql-tools';
import { getWikipediaMobileURL } from '../lib/utils';

const type : IResolvers = {
    Season:{
        year:parent=> parent.season,
        urlMobile:parent=> getWikipediaMobileURL(parent.url)
    },
    Race:{
        name: parent=> parent.raceName,
        circuit:parent=>parent.Circuit,
        urlMobile:parent=> getWikipediaMobileURL(parent.url)

    },
    Circuit:{
        id:parent=>parent.circuitId,
        name:parent=>parent.circuitName,
        location:parent=>parent.Location,
        urlMobile:parent=> getWikipediaMobileURL(parent.url)

    },
    Location:{
        lng:parent=>parent.long,
        
    },
    Driver:{
        id:parent=>parent.driverId,
        name:parent=>parent.givenName.concat(' ').concat(parent.familyName),
        urlMobile:parent=> getWikipediaMobileURL(parent.url)
        
    },
    DriverStanding:{
        driver:parent=>parent.Driver,
        constructors:parent=>parent.Constructors
    },
    Constructor:{
        id:parent=>parent.constructorId
    }
};

export default type;