/*
 * @Author: Your name
 * @Date:   2020-04-20 21:27:16
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-21 01:50:44
 */
// Añadir los imports
import express from 'express';
import compression from 'compression';
import cors from 'cors';
import schema from './schema';
import { ApolloServer } from 'apollo-server-express';
import { createServer } from 'http';
import expressPlayGround from 'graphql-playground-middleware-express';
import { dataSource } from './data';

async function init() {
    // Inicializamos la aplicación express
    const app = express();

    // Añadimos configuración de Cors y compression
    app.use('*', cors());

    app.use(compression());

    // Inicializamos el servidor de Apollo
    const server = new ApolloServer({
        schema,
        introspection: true, // Necesario
        dataSources:()=>({
            seasons: new dataSource.SeasonsData(),
            races: new dataSource.RacesData(),
            drivers: new dataSource.DriversData(),
            circuits:new dataSource.CircuitsData()
        })
    });

    server.applyMiddleware({ app });

    app.use('/', expressPlayGround({
        endpoint: '/graphql'
    }));

    const PORT = process.env.PORT || 8089;

    const httpServer = createServer(app);

    httpServer.listen({ port: PORT }, (): void => console.log(`http://localhost:${PORT}/graphql`));
}

init();
